// gcc gauss.c -o gauss_5 -lm && ./gauss_5

#include <stdio.h>
#include <math.h>

#define N 4 // ������ �������

// ������� ��� ���������� ������ ������
void gauss(double A[N][N + 1], double X[N])
{
    for (int i = 0; i < N; i++)
    {
        // ����� ������ � ������������ A[i][i]
        int max = i;
        for (int j = i + 1; j < N; j++)
            if (fabs(A[j][i]) > fabs(A[max][i]))
                max = j;

        // ������������ �����
        if (max != i)
        {
            for (int k = 0; k < N + 1; k++)
            {
                double temp = A[i][k];
                A[i][k] = A[max][k];
                A[max][k] = temp;
            }
        }

        // ��������� ��������� ���� i
        for (int j = i + 1; j < N; j++)
        {
            double factor = A[j][i] / A[i][i];
            for (int k = i; k < N + 1; k++)
                A[j][k] -= factor * A[i][k];
        }
    }

    // �������� ������ ��� ���������� X
    for (int i = N - 1; i >= 0; i--)
    {
        X[i] = A[i][N] / A[i][i];
        for (int j = 0; j < i; j++)
            A[j][N] -= A[j][i] * X[i];
    }
}

int main()
{
    double A[N][N + 1] = {
        {2.37, 0.93, 0.35, 0.18, 3.5},
        {0.93, 3.46, 0.69, 0.23, 5.07},
        {0.35, 0.69, 5.30, 0.91, 6.56},
        {0.18, 0.23, 0.91, 7.05, 8.96}};
    double X[N];

    gauss(A, X);

    printf("Resheniye sistemy: \n");
    for (int i = 0; i < N; i++)
        printf("X[%d] = %f\n", i, X[i]);

    printf("Proverka resheniya:\n");
    for (int i = 0; i < N; i++)
    {
        double sum = 0.0;
        for (int j = 0; j < N; j++)
            sum += A[i][j] * X[j];
        printf("B[%d] = %f\n", i, sum);
    }

    return 0;
}
